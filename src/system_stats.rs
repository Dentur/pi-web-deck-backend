use serde::{Serialize};
use std::time::{SystemTime, UNIX_EPOCH};
use sysinfo::{System, SystemExt, DiskExt, ComponentExt, ProcessorExt};
use std::sync::{Arc, Mutex};

#[derive(Serialize, Clone)]
pub struct Disk {
    name: String,
    mount: String,
    available: u64,
    total: u64
}

#[derive(Serialize, Clone)]
pub struct Memory{
    total: u64,
    used: u64
}

#[derive(Serialize, Clone)]
pub struct  Cpu{
    usage: f32
}

#[derive(Serialize, Clone)]
pub struct  Component{
    label: String,
    temperature: f32
}

#[derive(Serialize, Clone)]
pub struct Stats {
    time: u128,
    uptime: u64,
    disks: Vec<Disk>,
    memory: Memory,
    cpus: Vec<Cpu>,
    components: Vec<Component>
}

#[derive(Clone)]
pub struct SystemStats{
    system: Arc<Mutex<System>>
}

impl SystemStats{
    pub fn new() -> SystemStats {
        let mut system = System::new_all();
        system.refresh_all();
        return SystemStats{
            system: Arc::new(Mutex::new(system))
        };
    }

    pub fn assemble_stats(& self) -> Stats{
        let time = SystemTime::now().duration_since(UNIX_EPOCH).expect("Something is really wrong with the time...").as_millis();
        let mut system = self.system.lock().expect("Could not lock system info");
        system.refresh_all();

        let uptime = system.get_uptime();

        let mut disks: Vec<Disk> = Vec::new();
        for disk in system.get_disks() {
            disks.push(Disk{
                name: String::from(disk.get_name().to_str().expect("Drive name OsString could not be converted to str")),
                mount: String::from(disk.get_mount_point().to_str().expect("Mount point Path could not be converted to str")),
                total: disk.get_total_space(),
                available: disk.get_available_space()
            })
        }

        let memory = Memory{
            total: system.get_total_memory(),
            used: system.get_used_memory()
        };

        let mut components : Vec<Component> = Vec::new();
        for component in system.get_components() {
            components.push(Component {
                    label: String::from(component.get_label()),
                    temperature: component.get_temperature()
                });
        }

        let mut cpus : Vec<Cpu> = Vec::new();
        for processor in system.get_processors() {
            cpus.push(Cpu{
                usage: processor.get_cpu_usage()
            })
        }

        return Stats{
            time,
            uptime,
            memory,
            cpus,
            disks,
            components
        }
    }

}