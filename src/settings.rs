use serde::{Deserialize, Serialize};
use config::{ConfigError, Config, File};
use std::path::{Path, PathBuf};
use std::fs;
use std::io::Write;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct MqttSettings{
    pub enable: bool,

    pub client_id: String,
    pub client_user: String,
    pub client_password: String,

    pub server_url: String,
    pub mqtt_port: u16
}

impl Default for MqttSettings{
    fn default() -> Self {
        return MqttSettings{
            enable: false,

            client_id: "".to_string(),
            client_user: "".to_string(),
            client_password: "".to_string(),

            server_url: "".to_string(),
            mqtt_port: 8883
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DiscordSettings{
    pub token: String,
    pub bot_name: String
}

impl Default for DiscordSettings{
    fn default() -> Self {
        return DiscordSettings {
            token: "".to_string(),
            bot_name: "Pi Web Deck Bot".to_string()
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Settings{
    pub debug: bool,
    pub discord: DiscordSettings,
    pub mqtt: MqttSettings
}

impl Default for Settings {
    fn default() -> Self {
        return Settings{
            debug: false,
            discord: DiscordSettings::default(),
            mqtt: MqttSettings::default()
        }
    }
}

impl Settings{
    pub fn new(app_dir: PathBuf) -> Result<Self, ConfigError>{
        let mut s = Config::new();


        let app_config = format!("{}/config.toml", app_dir.clone().to_str()
            .expect("Could not parse app_dir"));

        log::trace!("Setting file {}", app_config.clone());

        //Create a new Config file, if the there is none
        if !Path::new(app_config.clone().as_str()).exists(){
            log::info!("Config file is not present at {}. Generating new Logfile", app_config.clone());
            let file_content = toml::to_string_pretty(&Settings::default())
                .expect("Could not Serialize Settings");

            let mut config_file = fs::File::create(app_config.clone())
                .expect("Could not write config File!");
            config_file.write_all(file_content.as_bytes());
            log::info!("Config file created")
        }

        s
            .merge(File::with_name(app_config.as_str())).unwrap()
            .merge(config::Environment::with_prefix("APP")).unwrap();


        // You can deserialize (and thus freeze) the entire configuration as
        s.try_into()
    }
}