#[macro_use]
extern crate dotenv;
extern crate app_dirs;
extern crate ctrlc;

use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use actix_cors::Cors;
use log::{info};
use flexi_logger;
use flexi_logger::{Logger, Duplicate};
use app_dirs::*;
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::{Connection};
use r2d2::Pool;
use actix_web::web::{PayloadConfig};
use sysinfo::{SystemExt};
use crate::system_stats::SystemStats;
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread::{sleep, JoinHandle};
use std::time::Duration;
use std::sync::{Arc, RwLock, Mutex};
use config::Config;
use crate::settings::{Settings, MqttSettings};
use std::process::exit;
use crate::mqtt_connection::MqttConnection;

mod routes;
pub mod system_stats;
pub mod discord_connection;
pub mod settings;
pub mod mqtt_connection;

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("<html><body><h3>API Base for the Stream Deck PI version </hr><br/> Happy Coding  💻</body></html>")
}

const APP_INFO: AppInfo = AppInfo{name: "stream-deck-pi", author: "Dentur"};

#[derive(Clone)]
pub struct AppState {
    db: Pool<SqliteConnectionManager>,
    system_stats: SystemStats,
    settings: Settings,
    mqtt_connection: Option<MqttConnection>
}

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!("migrations");
}

include!(concat!(env!("OUT_DIR"), "/generated.rs"));

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    let running = Arc::new(AtomicBool::new(true));

    Logger::with_str("debug")
        .log_to_file()
        //.directory("logs")
        .directory(get_app_root(AppDataType::UserConfig, &APP_INFO).unwrap())
        .duplicate_to_stdout(Duplicate::All)
        .duplicate_to_stderr(Duplicate::Warn)
        .start()
        .expect("Could not create Logger!");

    let settings = match Settings::new(get_app_root(AppDataType::UserConfig, &APP_INFO).unwrap()){
        Ok(set)=> set,
        Err(error) =>{
            log::error!("Could not load application settings file \n {}", error.to_string());
            panic!("Could not load settings!")
        }
    };
    info!("Settings loaded");

    std::env::set_var("RUST_LOG", "actix_web=trace");
    info!("Starting stream-deck-pi Server");

    let mut database_url_buf = get_app_root(AppDataType::UserConfig, &APP_INFO).unwrap();
    database_url_buf.push("stream-deck-pi.sqlite");
    let database_url = database_url_buf.into_os_string();
    info!("DB location: {}", database_url.clone().into_string().unwrap());

    info!("DB Migration starting");
    let mut tmp_conn = Connection::open(database_url.clone()).unwrap();
    embedded::migrations::runner().run(&mut tmp_conn).expect("DB Migration failed");
    tmp_conn.close().expect("DB Migration connection did not close!");
    info!("DB Migration finished");

    let manager = SqliteConnectionManager::file(database_url.clone());
    let pool = r2d2::Pool::new(manager).unwrap();

    let mut mqtt_connection: Option<MqttConnection> = None;
    let mut mqtt_thread_join: Option<JoinHandle<()>> = None;
    if settings.mqtt.enable {
        info!("MQTT connecting");
        let (con, thread_join) = MqttConnection::new(settings.clone(), running.clone()).expect("Could not initialize mqtt connection");

        mqtt_connection.replace(con);
        mqtt_thread_join.replace(thread_join);
        info!("MQTT connected");
    }

    let app_state = AppState{
        db: pool.clone(),
        system_stats:SystemStats::new(),
        settings: settings,
        mqtt_connection: mqtt_connection

    };


    let http_server = HttpServer::new(move ||{
        let generated = generate();
        App::new()
            .data(PayloadConfig::new(1024*1024*1024))//Set Payload (Upload) Limit to 1GB
            .data(web::JsonConfig::default().limit(1024 * 1024 * 1024))//Set JSON Decode Limit to 1GB
            .wrap(Cors::permissive())
            .data(app_state.clone())
            .configure(routes::configure_routes)
            .service(hello)
            .service(actix_web_static_files::ResourceFiles::new(
                "/frontend", generated,
            ))
    }).workers(1)
        .bind("192.168.188.70:8085").expect("Could not create http_server");

    let running_http_server = http_server.run();

    let ctrlc_running = running.clone();
    ctrlc::set_handler(move || {
        //Close any threads here...
        ctrlc_running.store(false, Ordering::SeqCst);
        return ();
    }).expect("Could not initialize ctrlc handler");


    while running.load(Ordering::SeqCst) {
        sleep(Duration::from_millis(500));
    }

    log::info!("Shutting down Server");
    running_http_server.stop(true);
    if mqtt_thread_join.is_some(){
        mqtt_thread_join.take().map(JoinHandle::join);
    }
    log::debug!("actix stopped");
    log::info!("Server Stopped");
    sleep(Duration::from_secs(1));
    return Ok(())
}
