use serde::{Serialize, Deserialize};
use crate::settings::Settings;
use rumqttc::{MqttOptions, Client, QoS, Connection, AsyncClient};
use std::sync::{Mutex, Arc};
use std::thread;
use std::time::Duration;
use std::thread::JoinHandle;
use serenity::static_assertions::_core::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;


#[derive(Clone)]
pub struct MqttConnection {
    client: Client
}

impl MqttConnection {
    pub fn new(settings:Settings, running: Arc<AtomicBool>) -> Result<(MqttConnection, JoinHandle<()>), String>{
        let mut options = MqttOptions::new(settings.mqtt.client_id.clone(),
                                           settings.mqtt.server_url.clone(),
                                           settings.mqtt.mqtt_port.clone());

        if(!settings.mqtt.client_password.is_empty() && !settings.mqtt.client_user.is_empty()){
            options.set_credentials(settings.mqtt.client_user.clone(), settings.mqtt.client_password.clone());
        }
        else{
            log::info!("MQTT: No credentials specified");
        }
        options.set_connection_timeout(30);
        let (mut client, mut connection) = Client::new(options, 10);

        let poll_thread = thread::spawn(move || {
            for (i, notification) in connection.iter().enumerate() {
                log::debug!("Received notification {:?}", notification);
                thread::sleep(Duration::from_millis(100));
            }
        });


        return Ok((MqttConnection{
            client
        }, poll_thread));
    }

    /*pub fn start_event_loop(&mut self, running: Arc<AtomicBool>){
        let connection_mutex = self.connection.clone();
        self.thread.replace(Arc::new(Mutex::new(thread::spawn(move || {
            loop{
                if running.load(Ordering::SeqCst) {
                    break;
                }
                {
                    let mut connection = &*connection_mutex.lock().expect("Could not lock mqtt connection!");
                    connection.eventloop.poll();
                }
                thread::sleep(Duration::from_millis(100));
            }
        }))));
    }*/

    pub  fn send_message(&mut self, topic: &str, message: &str) -> Result<(), String>{
        match self.client.publish(topic, QoS::AtLeastOnce, false, message) {
            Ok(()) =>return Ok(()),
            Err(err) =>{
                log::error!("Error while publishing message {} to {}. Parent error {}", message.clone(), topic.clone(), err.to_string());
                return Err(format!("Error while publishing message {} to {}", message, topic))
            },
        }
    }
}
