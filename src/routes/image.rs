use actix_web::{web, HttpResponse, Result};
use serde::*;
use crate::AppState;
use log;
use rusqlite::{params};
use uuid::Uuid;
use crate::routes::error_message::ErrorMessage;


#[derive(Deserialize)]
struct AddImageSchema {
    data: String,
    name: String
}

#[derive(Serialize)]
struct AddImageResponse{
    id: String
}

async fn add_image(state: web::Data<AppState>, add_image_schema: web::Json<AddImageSchema>) ->Result<HttpResponse>{
    log::debug!("add_image start");
    let db_conn = state.db.get().unwrap();
    let id = Uuid::new_v4();
    let id_string = id.to_hyphenated();
    if let Err(e) = db_conn.execute(
        "INSERT INTO images (id, name, data) VALUES (?1, ?2, ?3);",
        params![id_string.clone().to_string(), add_image_schema.name, add_image_schema.data]
    ){
        log::error!("Could not insert Image {} with id {}", add_image_schema.name, id_string);
        return Ok(HttpResponse::InternalServerError().json(ErrorMessage{
            error_message: e.to_string()
        }))
    }

    return Ok(HttpResponse::Ok().json(AddImageResponse{
        id: id_string.to_string()
    }));
}

#[derive(Serialize, Deserialize)]
struct Image{
    id: String,
    data: String,
    name: String
}

#[derive(Serialize)]
struct ImageArray{
    images: Vec<Image>
}

async fn get_images(state: web::Data<AppState>) -> Result<HttpResponse>{
    log::debug!("get_images start");
    let db_conn = state.db.get().unwrap();
    let mut stmt = db_conn.prepare("SELECT id, name, data FROM images")
        .expect("Could not get images");
    let image_result = stmt.query_map(params![], |row| {
        Ok(Image {
            id: row.get(0)?,
            name: row.get(1)?,
            data: row.get(2)?,
        })
    });
    if let Err(e) = image_result {
        log::error!("Could not get Images");
        return Ok(HttpResponse::InternalServerError().json(ErrorMessage{
            error_message: e.to_string()
        }))
    }
    let images = image_result.unwrap()
        .map(|image_result|{image_result.unwrap()}).collect::<Vec<Image>>();

    return Ok(HttpResponse::Ok().json(ImageArray{images }))
}

async fn delete_image(db: web::Data<AppState>, web::Path(image_id): web::Path<String>) -> Result<&'static str>{
    log::debug!("delete_image start");
    return Ok("Not Implemented delete");

}

#[derive(Deserialize)]
struct UpdateImageSchema {
    name: String
}

async fn update_image(db: web::Data<AppState>,
                      web::Path(image_id): web::Path<String>,
                      update_image_schema: web::Json<UpdateImageSchema>) -> Result<&'static str>{
    log::debug!("update_image start");
    return Ok("Not Implemented update");

}

pub fn config_route_image(cfg: &mut web::ServiceConfig){
    cfg.service(
        web::resource("/images")
            .route(web::get().to(get_images))
            .route(web::post().to(add_image))
    ).service(
        web::resource("/image/{imageId}")
            .route(web::put().to(update_image))
            .route(web::delete().to(delete_image))
    );
    log::info!("Route: /images [GET, PUSH] available");
    log::info!("Route: /image/<image_id> [UPDATE, DELETE] available");
}
