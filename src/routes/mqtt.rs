use actix_web::{web, HttpResponse, Result};
use crate::AppState;
use crate::routes::error_message::ErrorMessage;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct MqttSendOptions {
    topic: String,
    message: String
}

async fn mqtt_send(state: web::Data<AppState>, mqtt_send_options: web::Json<MqttSendOptions>) -> Result<HttpResponse>{
    log::debug!("Mqtt Action start");
    return if state.mqtt_connection.is_none() {
        Ok(HttpResponse::ServiceUnavailable().json(ErrorMessage {
            error_message: "mqtt service is disabled".to_string()
        }))
    } else {
        if let Some(ref mut connection) = state.mqtt_connection.clone(){
            match connection.send_message(mqtt_send_options.topic.as_str(), mqtt_send_options.message.as_str()) {
                Ok(()) => Ok(HttpResponse::Ok().finish()),
                Err(error) => Ok(HttpResponse::BadGateway().json(ErrorMessage { error_message: error.to_string() }))
            }
        }
        else{
            return Ok(HttpResponse::BadGateway().finish());
        }

    }

}

async fn mqtt_available(state: web::Data<AppState>) -> Result<HttpResponse>{
    log::debug!("Mqtt Available start");
    return if state.mqtt_connection.is_some(){
        Ok(HttpResponse::Ok().finish())
    }
    else{
        Ok(HttpResponse::ServiceUnavailable().finish())
    }
}

pub fn config_route(cfg: &mut web::ServiceConfig){
    cfg.service(
        web::resource("/mqtt")
            .route(web::post().to(mqtt_send))
            .route(web::get().to(mqtt_available))
    );
    log::info!("Route: /mqtt [POST, GET] available");
}