pub mod image;
pub mod panels;
pub mod action;
pub mod stats;
pub mod error_message;
pub mod mqtt;
use actix_web::{web};


pub fn configure_routes(cfg: &mut web::ServiceConfig) {
    image::config_route_image(cfg);
    panels::config_route_panels(cfg);
    action::config_route(cfg);
    stats::config_route(cfg);
    mqtt::config_route(cfg);
}