use actix_web::{web, HttpResponse, Result};
use crate::AppState;
use uuid::Uuid;
use serde::{Serialize, Deserialize};
use rusqlite::{params};
use crate::routes::error_message::ErrorMessage;

#[derive(Serialize, Deserialize)]
struct Panel{
    id: String,
    data: String
}

#[derive(Deserialize)]
struct PanelData {
    data: String
}

#[derive(Deserialize, Serialize)]
struct AddPanelResult{
    id: String
}

async fn add_panel(state: web::Data<AppState>, add_panel: web::Json<PanelData>) -> Result<HttpResponse>{
    log::debug!("add_panel start");
    let db_conn = state.db.get().unwrap();
    let id = Uuid::new_v4();
    let id_string = id.to_hyphenated();
    if let Err(e) = db_conn.execute(
        "INSERT INTO panels (id, data) VALUES (?1, ?2);",
        params![id_string.clone().to_string(), add_panel.data]
    ){
        log::error!("Could not insert Panel");
        return Ok(HttpResponse::InternalServerError().json(ErrorMessage{
            error_message: e.to_string()
        }))
    }

    return Ok(HttpResponse::Ok().json(AddPanelResult{
        id: id_string.to_string()
    }));
}

#[derive(Serialize)]
struct PanelArray{
    panels: Vec<Panel>
}

async fn get_panels(state: web::Data<AppState>) -> Result<HttpResponse>{
    log::debug!("get_panels start");
    let db_conn = state.db.get().unwrap();
    let mut stmt = db_conn.prepare("SELECT id, data FROM panels")
        .expect("Could not create images get statement");
    let panel_results = stmt.query_map(params![], |row| {
        Ok(Panel {
            id: row.get(0)?,
            data: row.get(1)?,
        })
    });
    if let Err(e) = panel_results {
        log::error!("Could not get Panels");
        return Ok(HttpResponse::InternalServerError().json(ErrorMessage{
            error_message: e.to_string()
        }))
    }
    let panels = panel_results.unwrap()
        .map(|panel_result|{ panel_result.unwrap()}).collect::<Vec<Panel>>();

    return Ok(HttpResponse::Ok().json(PanelArray{ panels }))
}

async fn update_panel(state: web::Data<AppState>, web::Path(panel_id): web::Path<String>, panel_data: web::Json<PanelData>) -> Result<HttpResponse>{
    log::debug!("update_panel start");

    let db_conn = state.db.get().unwrap();
    if let Err(e) = db_conn.execute(
        "UPDATE panels SET data= ?2 WHERE id= ?1;",
        params![panel_id.clone(), panel_data.data]
    ){
        log::error!("Could not update Panel");
        return Ok(HttpResponse::InternalServerError().json(ErrorMessage{
            error_message: e.to_string()
        }))
    }

    return Ok(HttpResponse::Ok().finish());
}

async fn delete_panel(state: web::Data<AppState>, web::Path(panel_id): web::Path<String>) -> Result<HttpResponse>{
    log::debug!("delete_panel start");
    let db_conn = state.db.get().unwrap();
    if let Err(e) = db_conn.execute(
        "DELETE FROM panels WHERE id= ?1;",
        params![panel_id.clone()]
    ){
        log::error!("Could not delete Panel");
        return Ok(HttpResponse::InternalServerError().json(ErrorMessage{
            error_message: e.to_string()
        }))
    }

    return Ok(HttpResponse::Ok().finish());
}

pub fn config_route_panels(cfg: &mut web::ServiceConfig){
    cfg.service(
        web::resource("/panels")
            .route(web::get().to(get_panels))
            .route(web::post().to(add_panel))
    ).service(
        web::resource("/panel/{panel_id}")
            .route(web::put().to(update_panel))
            .route(web::delete().to(delete_panel))
    );
    log::info!("Route: /panels [GET, POST] available");
    log::info!("Route: /panel/<panel_id> [UPDATE, DELETE] available");
}