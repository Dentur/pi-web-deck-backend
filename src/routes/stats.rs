use actix_web::{web, HttpResponse, Result};
extern crate shell_words;
use crate::AppState;

async fn get_stats(state: web::Data<AppState>) -> Result<HttpResponse>{
    log::debug!("get_stats start");
    let system_stats = state.system_stats.clone();
    let stats = system_stats.assemble_stats();
    return Ok(HttpResponse::Ok().json(stats));
}

pub fn config_route(cfg: &mut web::ServiceConfig){
    cfg.service(
        web::resource("/stats")
            .route(web::get().to(get_stats))
    );
    log::info!("Route: /stats [GET] available");
}