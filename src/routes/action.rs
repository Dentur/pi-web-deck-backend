use actix_web::{web, HttpResponse, Result};
use serde::{Deserialize};
use std::process::Command;
extern crate shell_words;

#[derive(Deserialize)]
struct Action {
    action_type: String,
    command: String,
    options: String
}

const ACTION_TYPE_START_PROGRAM: &str = "START_PROGRAM";
const ACTION_TYPE_OPEN_WEB_PAGE: &str = "OPEN_WEB_PAGE";
const ACTION_TYPE_COMMAND: &str = "COMMAND";

fn handle_start_program(action: Action){
    log::trace!("handle_start_program start");
    let split_options ;
    match shell_words::split(action.options.clone().as_str()){
        Ok(result) => split_options = result,
        Err(error) => {
            log::warn!("Could not split options {}", action.options.clone());
            return;
        }
    }
    match Command::new(action.command.clone())
        .args(split_options)
        .spawn()
    {
        Ok(child) => return,
        Err(error) => log::error!("Could not start Program! {} \n {}", action.command.clone(), error)
    }
}

fn handle_open_web_page(action: Action){
    log::trace!("handle_open_web_page start");

}

fn handle_command(action: Action){
    log::trace!("handle_command start");
    let split_options ;
    match shell_words::split(action.options.clone().as_str()){
        Ok(result) => split_options = result,
        Err(error) => {
            log::warn!("Could not split options {}", action.options.clone());
            return;
        }
    }
    match Command::new("cmd")
        .args(&["/C", action.command.clone().as_str()])
        .args(split_options)
        .spawn()
    {
        Ok(child) => return,
        Err(error)=> log::error!("Could not execute command! {} \n {}", action.command.clone(), error)
    }
}


async fn action(action: web::Json<Action>) -> Result<HttpResponse>{
    log::debug!("action start");
    match action.action_type.as_str() {
        ACTION_TYPE_COMMAND => handle_command(action.into_inner()),
        ACTION_TYPE_OPEN_WEB_PAGE => handle_open_web_page(action.into_inner()),
        ACTION_TYPE_START_PROGRAM => handle_start_program(action.into_inner()),
        _ => log::warn!("Unrecognised action type {}", action.action_type.clone())
    }
    return Ok(HttpResponse::Ok().finish());
}

pub fn config_route(cfg: &mut web::ServiceConfig){
    cfg.service(
        web::resource("/action")
            .route(web::post().to(action))
    );
    log::info!("Route: /action [POST] available");
}