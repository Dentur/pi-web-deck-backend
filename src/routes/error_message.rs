use serde::{Serialize};

#[derive(Serialize)]
pub struct ErrorMessage {
    pub error_message: String
}