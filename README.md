# Pi Web Deck
Storage and Execution engine for the [Pi Web Deck Project](https://gitlab.com/Dentur/pi-web-deck).

This project aims at providing an alternative to the Stream Deck.
The Decks content can be edited in an Editor and are Displayed on a touch screen connected to a Raspberry Pi.

Buttons can execute bash or shell commands, open programs or websites and (maybe) much more. See below for a full list
of implemented Actions.

> ⚠ Currently, the backend server is not secured by a password or similar. As such i cannot recommend running this
> Software on **Any** device directly conncted to the internet (without a nat and or firewall) or public networks.
> **Use on your own risk**

![Project Overview](./readme/Overview.png)

## Build Requirements
- (Build only) The Backend Server requires [Rustlang](https://www.rust-lang.org/tools/install)
- Building the Frontend requires [Yarn](https://yarnpkg.com/)

## Installation from Source
1. Clone this Repository by using

`git clone https://gitlab.com/Dentur/pi-web-deck-backend.git --recurse-submodules`

2. Build the project
`cargo build --release`

3. Run the Executable `stream-deck-pi-backend`. This will create a Server Listening on Port 8085. The Frontend can be 
   accessed locally via `<your ip>:8085/frontend/index.html`.
   
4. (Somewhat optional) For Optimal use, the executable should be run on boot / login. On Windows, this may be done help 
of the autostart folder.

## Configuration
Some features require configuration. The main way to configure the server is the configuration file.
Under Windows, it is located at `%AppData%/Dentur/stream-deck-pi/config.toml`. Under Linux, it should be 
stored at `$HOME/.config/Dentur/stream-deck-pi/config.toml`

This configuration is generated at the first start of the application. All configuration options should be inserted
on first start, but most quite possible contain no data.

### General configuration
At the start of the configuration file, general settings can be set.

#### debug
The `debug` settings allows the user to start the application with additional debug information enabled.

###[mqtt]
Under this section, a single mqtt server can be configured.
The `enable` option can be set to `true` or `false` to enable the mqtt service.
To connect to the server the `server_url` and `mqtt_port` need to be set.
Additionally, the client can be set be setting the `client_id`.
If the server requires authentication, the `client_user` and `client_password`fields need to be set. If the server 
requires no authentication, these can be left as empty strings.

###[discord] (wip)
This section will contain all data needed to connect this server to discord via a discord app. Currently, this feature 
is not enabled!

## Data Storage
The Directory used to store data is the userConfig Directory specified by [app_dirs](https://crates.io/crates/app_dirs).
On Windows the path 
is `%AppData%/Dentur/stream-deck-pi`. On *nix Systems it should be `$HOME/.config/Dentur/stream-deck-pi`.

The Data is stored in a sqlite Database. Additionally, logs can be found in this directory to.