use actix_web_static_files::{NpmBuild, resource_dir};

fn main() {
    NpmBuild::new("./stream-deck-pi-frontend")
        .executable("yarn")
        .install().unwrap()
        .run("build").unwrap()
        .target("./stream-deck-pi-frontend/build")
        .to_resource_dir()
        .build().unwrap();
    //resource_dir("./static").build().unwrap();
}