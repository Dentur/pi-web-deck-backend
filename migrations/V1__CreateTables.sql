CREATE TABLE images (
                        id TEXT PRIMARY KEY NOT NULL,
                        name TEXT NOT NULL,
                        data TEXT NOT NULL
);